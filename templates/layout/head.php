<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amazon</title>
    <!-- bootstrap, font awesome and jquery-ui css -->
    <link href="/Amazon/app/assets/3rd-party/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/Amazon/app/assets/3rd-party/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/Amazon/app/assets/3rd-party/jquery/jquery-ui.css" rel="stylesheet">

    <!-- custom css files -->
    <link href="/Amazon/app/assets/css/utilities.css" rel="stylesheet">
    <link href="/Amazon/app/assets/css/style.css" rel="stylesheet">

    <!-- bootstrap, jquery and jquery-ui js -->
    <script src="/Amazon/app/assets/3rd-party/jquery/jquery.js"></script>
    <script src="/Amazon/app/assets/3rd-party/jquery/jquery-ui.js"></script>
    <script src="/Amazon/app/assets/3rd-party/bootstrap/js/bootstrap.min.js"></script>

    <!-- custom javascript -->
    <script src="/Amazon/app/assets/js/main.js"></script>
</head>

