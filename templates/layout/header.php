<?php
if (isset($_SESSION['cart'])) {
    $cart = $_SESSION['cart'];
} else {
    $cart = [];
}

$count = count($cart);
?>

<nav id="header" class="navbar navbar-inverse navbar-fixed-top squared-corner">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/Amazon/app">Amazon</a>

            <form id="filters-form" class="navbar-form navbar-left"
                  action="<?= isset($actionUrl) ? $actionUrl : DEFAULT_SEARCH_URL ?>" onsubmit="submitFilters();">
                <input id="page" type="hidden" name="page" value="<?= isset($_GET['page']) ? $_GET['page'] : '' ?>">
                <input id="sort" type="hidden" name="sort" value="<?= isset($_GET['sort']) ? $_GET['sort'] : 'name' ?>">
                <input id="order" type="hidden" name="order"
                       value="<?= isset($_GET['order']) ? $_GET['order'] : 'asc' ?>">

                <input id="price_from" type="hidden" name="price_from"
                       value="<?= isset($_GET['price_from']) ? $_GET['price_from'] : '' ?>">
                <input id="price_to" type="hidden" name="price_to"
                       value="<?= isset($_GET['price_to']) ? $_GET['price_to'] : '' ?>">
                <input id="category" type="hidden" name="category"
                       value="<?= isset($_GET['category']) ? $_GET['category'] : '' ?>">

                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <input id="search" type="text" class="form-control" placeholder="Search" name="q"
                               value="<?= isset($_GET['q']) ? $_GET['q'] : '' ?>">
                        <span class="input-group-btn">
                            <button type="submit" id="submitButton" class="btn btn-default">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="menu">
            <ul class="nav navbar-nav navbar-right">
                <li id="cart"><a href="/Amazon/app/buy/cart.php">Cart <span class="cart-count"><?= $count ?></span></a>
                </li>
                <?php if (User::isGuest()): ?>
                    <li><a href="/Amazon/app/accounts/register.php"><span class="glyphicon glyphicon-user"></span> Sign
                            Up</a></li>
                    <li><a href="/Amazon/app/accounts/login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a>
                    </li>
                <?php else: ?>
                    <li><a href="/Amazon/app/items/orders.php">Orders</a></li>
                    <li><a href="/Amazon/app/my-store/index.php">My store</a></li>
                    <li><a href="/Amazon/app/accounts/logout.php"><span class="glyphicon glyphicon-log-out"></span>Logout</a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>

<?php foreach ($_SESSION['message'] as $type => $messages): ?>
    <?php foreach ($messages as $message): ?>
        <div class="alert alert-<?= $type ?> text-center" role="alert">
            <p><?= $message ?></p>
        </div>
    <?php endforeach; ?>
<?php endforeach; ?>

<?php
// reset session messages
$_SESSION['message'] = [];
?>