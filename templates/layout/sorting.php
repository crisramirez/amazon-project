<?php
$sort = isset($data['sort']) ? $data['sort'] : 'name';
$order = isset($data['order']) ? $data['order'] : 'asc';
?>
<div class="hidden-sm hidden-md hidden-lg">
    <button class="btn btn-link bold" onclick="toggleFilters(this);">
        Refine Search <i class="fa fa-caret-down"></i>
    </button>
</div>
<div id="sidebar" class="hidden-xs">
    <div class="add-bottom">
        <div class="form-group">
            <label class="block add-bottom-sm">Sort:</label>
            <select name="sorting" class="form-control input-sm" id="sorting" onchange="sort();">
                <?php foreach (SORT_OPTIONS as $option) {
                    // list($value, $dataOrder, $label) = $option;
                    $value = $option[0];
                    $dataOrder = $option[1];
                    $label = $option[2];

                    $html = '<option value="' . $value . '" data-order="' . $dataOrder . '">' . $label . '</option>';
                    $html = new SimpleXMLElement($html);
                    if ($sort == $value && $order == $dataOrder) {
                        $html->addAttribute('selected', true);
                    }
                    echo $html->asXML();
                } ?>
            </select>
        </div>
    </div>
    <div class="add-bottom">
        <div class="form-group">
            <label class="block add-bottom-sm">Categories:</label>
            <select class="form-control input-sm" onchange="selectCategory(this.value);">
                <option value="">--Select one--</option>
                <?php foreach (CATEGORIES as $category) {
                    $html = '<option value="' . $category . '">' . $category . '</option>';
                    $html = new SimpleXMLElement($html);
                    if ($category == $data['category']) {
                        $html->addAttribute('selected', true);
                    }
                    echo $html->asXML();
                } ?>
            </select>
        </div>
    </div>
    <div class="add-bottom">
        <div class="form-group">
            <label class="block add-bottom-sm">Price:</label>
            <div class="add-bottom">
                <div>
                    <button type="button" class="btn btn-link plain" onclick="filterPrice(0,50);">Under $50</button>
                </div>
                <div>
                    <button type="button" class="btn btn-link plain" onclick="filterPrice(50,100);">From $50-$100</button>
                </div>
                <div>
                    <button type="button" class="btn btn-link plain" onclick="filterPrice(100,'');">Over $100</button>
                </div>
            </div>
            <div class="add-bottom">
                <div class="input-group input-group-sm">
                    <input id="price-from" class="form-control price-input" type="number" name="price_from"
                            placeholder="$00.00"
                            value="<?= isset($data['price_from']) ? $data['price_from'] : '' ?>">

                    <input id="price-to" class="form-control price-input" type="number" name="price_to" placeholder="$00.00"
                            value="<?= isset($data['price_to']) ? $data['price_to'] : '' ?>">

                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button" onclick="filterPrice($('#price-from').val(), $('#price-to').val())">
                                Go
                            </button>
                        </span>
                </div>
            </div>
        </div>
    </div>
</div>