<?php
if ($page > $lastPage) {
    return;
}
$pagesToShow = 9; // this should always be an odd number
if ($lastPage < $pagesToShow) {
    $pagesToShow = $lastPage;
}
$lowerBound = 1;
$upperBound = 1;
if ($page <= $pagesToShow) {
    $lowerBound = 1;
    $upperBound = $pagesToShow;
} elseif ($page >= $pagesToShow && $page <= $lastPage - $pagesToShow) {
    $diff = floor($pagesToShow / 2);
    $lowerBound = $page - $diff;
    $upperBound = $page + $diff;
} elseif ($page > $lastPage - $pagesToShow) {
    $lowerBound = $lastPage - $pagesToShow;
    $upperBound = $lastPage;
}

?>

<ul class="pagination pagination-sm no-top pull-left">
    <?php if ($page != 1) : ?>
        <li>
            <a href="#" onclick="paginate('<?= ($page - 1) ?>')">Previous</a>
        </li>
    <?php endif; ?>
    <?php for ($i = $lowerBound; $i <= $upperBound; $i++) : ?>
        <li<?= (($i == $page) ? ' class="active"' : ''); ?>>
            <a href="#" onclick="paginate('<?= $i ?>')"><?= $i ?></a>
        </li>
    <?php endfor; ?>
    <?php if ($page != $lastPage) : ?>
        <li>
            <a href="#" onclick="paginate('<?= ($page + 1) ?>')">Next</a>
        </li>
    <?php endif; ?>
</ul>

<select class="form-control input-sm pull-left inline-block" style="margin-left: 10px; width: auto;" onchange="paginate(this.value)">
    <option value="">--Go to page--</option>
    <?php for ($i = 1; $i <= $lastPage; $i++): ?>
        <option value="<?= $i ?>">Page <?= $i ?></option>
    <?php endfor; ?>
</select>