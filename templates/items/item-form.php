<div class="well">
    <div class="row">
        <form class="form-horizontal" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="control-label col-sm-2" for="name">Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" id="name" value="<?= $item['name']; ?>" placeholder="Enter Name">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="sel1">Category:</label>
                <div class="col-sm-10">
                    <select class="form-control" id="sel1" name="category">
                        <option value="">--Select One--</option>
                        <?php foreach (CATEGORIES as $category) {
                            $html = '<option value="' . $category . '">' . $category . '</option>';
                            $html = new SimpleXMLElement($html);
                            if ($category == $item['category']) {
                                $html->addAttribute('selected', true);
                            }
                            echo $html->asXML();
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="description">Description:</label>
                <div class="col col-sm-10">
                    <textarea class="form-control" name="description" id="description" rows="5" placeholder="Enter Description"><?= $item['description']; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="price">Price:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="price" id="price" value="<?= toDollars($item['price']); ?>" placeholder="$000.00">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="image">Select image</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" name="image" id="image">
                    <?php if (!$item['isNew']): ?>
                        <p class="text-muted text-small remove-bottom add-top-sm">Leave blank to leave the current image</p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">
                        <?= $item['isNew'] ? 'Add Item' : 'Update Item' ?>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
