<?php
$cssClass = 'col col-xs-12';
if ($inCart) {
    $cssClass = 'col col-xs-8 col-sm-9 col-md-10 col-xs-offset-4 col-sm-offset-3 col-md-offset-2 padding-fix';
}
?>
<div id="total-price-info" class="row add-bottom">
    <div class="<?= $cssClass ?> bold">
        <div class="inline">
            <span class="cart-price"><?= toDollars($total) ?></span>
        </div>
        <div class="pull-right">
            Quantity: <span class="cart-count"><?= $count ?></span>
        </div>
    </div>
</div>
