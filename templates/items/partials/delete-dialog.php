
<div id="delete-<?= $item['id'] ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please confirm</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this item?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <?php if ($ajax): ?>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteItem('<?= $item['id'] ?>')">
                        Delete
                    </button>
                <?php else: ?>
                    <form action="/Amazon/app/my-store/delete.php" method="post">
                        <input type="hidden" name="id" value="<?= $item['id'] ?>">
                        <button type="submit" class="btn btn-primary">
                            Delete
                        </button>
                    </form>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>