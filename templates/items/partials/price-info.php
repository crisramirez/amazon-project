<div class="clear">
    <?php if (User::isGuest() || !$owner): ?>
        <button type="button" class="btn btn-warning" onclick="addToCart('<?= $item['id'] ?>')">Add to cart</button>
    <?php elseif ($owner && $ownerCtrls): ?>
        <a href="/Amazon/app/my-store/update.php?id=<?= $item['id']; ?>">
            <button type="button" class="btn btn-default"> Update</button>
        </a>
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-<?= $item['id'] ?>">
            Delete
        </button>
    <?php endif; ?>
    <span class="pull-right"><?= toDollars($item['price']); ?></span>
</div>