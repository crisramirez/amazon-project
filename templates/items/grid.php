<?php
$owner = !User::isGuest() && User::getCurrentUserId() == $item['user_id'];
if (!isset($ownerCtrls)) {
    $ownerCtrls = false;
}
?>
    <div id="item-<?= $item['id'] ?>" class="col col-sm-6 col-lg-3 cart-item-wrapper">
        <div class="cart-item">
            <div>
                <a href="/Amazon/app/items/item.php?id=<?= $item['id'] ?>">
                    <div class="image-wrapper text-center">
                        <?php
                        list($w, $h) = getimagesize(CDN . 'images/' . $item['image']);

                        $imgClass = $w > $h ? 'landscape-img' : 'portrait-img';
                        ?>
                        <img id="img-<?= $item['id'] ?>" class="<?= $imgClass ?>" src="/Amazon/app/assets/images/<?= $item['image'] ?>">
                    </div>
                </a>
                <a title="<?= $item['name'] ?>" href="/Amazon/app/items/item.php?id=<?= $item['id'] ?>">
                    <h3 class="no-top add-bottom-xs block one-liner"><?= substr($item['name'], 0, 20); ?></h3>
                </a>
                <span class="text-muted add-bottom text-small block"><?= $item['category'] ?></span>
            </div>
            <?php
            TemplateManager::loadTemplate('/items/partials/price-info.php', [
                'owner' => $owner,
                'ownerCtrls' => $ownerCtrls,
                'item' => $item
            ]);
            ?>
        </div>
    </div>

<?php
if ($owner && $ownerCtrls) {
    TemplateManager::loadTemplate('/items/partials/delete-dialog.php', [
        'item' => $item,
        'ajax' => true
    ]);
}
?>