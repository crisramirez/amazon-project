<?php
$cssClass = 'col col-xs-12';
if ($inCart) {
    $cssClass = 'col col-xs-9 col-sm-9 col-md-10';
}
?>
<div id="item-<?= $item['id'] ?>" class="cart-item-line clear">
    <div class="row add-padding-top add-bottom">
        <?php if ($inCart): ?>
            <div class="col col-xs-3 col-sm-3 col-md-2">
                <img src="/Amazon/app/assets/images/<?= $item['image'] ?>">
            </div>
        <?php endif; ?>
        <div class="<?= $cssClass ?>">
            <h3 class="no-top"><?= $item['name']; ?></h3>
            <div>
                <span><?= toDollars($item['price']) ?></span>
                <span class="pull-right">Quantity: <?= $item['count'] ?></span>
            </div>
        </div>
    </div>
    <div class="clear add-bottom text-right">
        <?php if ($inCart): ?>
            <button class="btn btn-primary" onclick="removeFromCart('<?= $item['id'] ?>')">
                Remove
            </button>
        <?php endif; ?>
    </div>
</div>