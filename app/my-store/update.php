<?php
include_once '../../autoload.php';

$id = $_GET['id'];

$sql = "SELECT * FROM `catalog` WHERE `id` = ?";
$item = DB::fetch($sql, [
    $id
]);
$item['isNew'] = false;

if (count($_POST) > 0) {
    // todo basic validation
    // you already have the item's info in $row
    // check for all the inputs and if it anything is not submitted then
    // set the value equals to whatever you had before the update to
    // avoid overriding the values to empty

    // if the file wasn't uploaded then the size of $_FILES['image'] will be zero
    // if that is the case, use the image already saved
    if (empty($_FILES) || empty($_FILES['image']['size'])) {
        $image = $item['image'];
    } else {
        if ($_FILES['image']['error'] > 0) {
            throw new Exception('Upload unsuccessful.');
        }

        $image = transformImageName($_FILES['image']['name']);

        $oldpath = $_FILES['image']['tmp_name'];
        $newpath = "../assets/images/" . $image;

        $uploaded = move_uploaded_file($oldpath, $newpath);
        if (!$uploaded) {
            throw new Exception('Upload unsuccessful.');
        }
    }

    // todo: check if all fields are submitted. Check the value was submitted and that it's not empty

    $name = $_POST['name'];
    $category = $_POST['category'];
    $description = $_POST['description'];
    // remove anything that's not a number or a period
    // and then get cast it into a float
    // @link http://php.net/manual/en/function.floatval.php and multiple by 100
    // then round off any excess decimals
    $price = floatval(preg_replace('/[^0-9,.]/', '', $_POST['price'])) * 100;
    $price = floor($price);

    $sql = "UPDATE `catalog` SET `name` = ?, `category` = ?,
           `description` = ?, `price` = ?, `image` = ?
            WHERE `id` = ?";

    DB::execute($sql, [
        $name,
        $category,
        $description,
        $price,
        $image,
        $id
    ]);
    header("Location: ./index.php"); // view of all your items
    exit();
}

/**
 * HOW TO pre-select from a dropdown
 * 1) make your list dynamic, as in an array in php, that way you can add and remove things easily
 * 2) make a foreach when creating your select box foreach ($array as $key => $value)
 *    your key is the value of the <option> and the $value is the label being displayed
 *    if your array is a numeric array (i.e [abc, def, ghi]) then $value is both the value and label
 * 3) as you go through your foreach, check if the $value matches the item's category
 *    if it does, add the 'selected' attribute to the <option>
 *
 * For the file
 * On submit, check if an image has been uploaded
 * if it hasn't do not add that part to the query
 */
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Amazon | Update Item</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div class="container">
        <h1>Update Item</h1>
        <div class="row">
            <?php
            TemplateManager::loadTemplate('/items/item-form.php', [
                'item' => $item
            ]);
            ?>
        </div>
    </div>

    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>

