<?php
include_once '../../autoload.php';

// something was submitted 
if (count($_POST) > 0) {

    if (count($_FILES) == 0) {
        throw new Exception('Item image is required.');
    }
    if ($_FILES['image']['error'] > 0) {
        throw new Exception('Upload unsuccessful.');
    }

    $image = transformImageName($_FILES['image']['name']);

    $oldpath = $_FILES['image']['tmp_name'];
    $newpath = "../assets/images/" . $image;

    $uploaded = move_uploaded_file($oldpath, $newpath);
    if (!$uploaded) {
        throw new Exception('Upload unsuccessful.');
    }
    // todo: check if all fields are submitted. Check the value was submitted and that it's not empty

    $name = $_POST['name'];
    $category = $_POST['category'];
    $description = $_POST['description'];
    $userId = User::getCurrentUserId();
    // remove anything thats not a number or a period
    // and then get cast it into a float
    // @link http://php.net/manual/en/function.floatval.php and multiple by 100
    // then round off any excess decimals
    $price = floatval(preg_replace('/[^0-9,.]/', '', $_POST['price'])) * 100;
    $price = floor($price);

    $sql = "INSERT INTO `catalog` (`user_id`,`name`,`category`,`description`,`image`,`price`)
            VALUES(?,?,?,?,?,?)";

    DB::execute($sql, [
        $userId,
        $name,
        $category,
        $description,
        $image,
        $price
    ]);
    header("Location: ./index.php"); // view of all your items
    exit();
}

// if $_GET request then display form
// create empty item for the form
$item = [
    'name' => '',
    'description' => '',
    'category' => '',
    'price' => '',
    'image' => '',
    'isNew' => true
];
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Amazon | Add New Item</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div class="container">
        <h1>Add Item</h1>
        <?php
        TemplateManager::loadTemplate('/items/item-form.php', [
            'item' => $item
        ]);
        ?>
    </div>
    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>



