<?php
include_once '../../autoload.php';

$userId = User::getCurrentUserId();
$page = getPage();

$data = getItems($_GET, $page, $userId);

$items = $data['items'];
$lastPage = $data['lastPage'];

$actionUrl = '/Amazon/app/my-store/index.php';
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Amazon | My Items</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<?php
TemplateManager::loadTemplate('/layout/header.php', [
    'actionUrl' => $actionUrl
]);
?>

<div class="container">
    <h1>My Store</h1>
    <div class="add-bottom text-large">
        <a href="add.php">Add Item</a>
    </div>
    <?php if (count($items) == 0): ?>
        <h3>You are not selling anything.</h3>
    <?php else: ?>
        <div class="row">
            <div class="col-sm-3">
                <?php
                TemplateManager::loadTemplate('/layout/sorting.php', [
                    'data' => $_GET
                ]);
                ?>
            </div>
            <div class="col-sm-9">
                <div id="items-grid" class="row">
                    <?php
                    foreach ($items as $item) {
                        TemplateManager::loadTemplate('/items/grid.php', [
                            'item' => $item,
                            'ownerCtrls' => true
                        ]);
                    }
                    ?>
                </div>
                <div class="add-top add-bottom clear">
                    <?php
                    TemplateManager::loadTemplate('/layout/pagination.php', [
                        'page' => $page,
                        'lastPage' => $lastPage
                    ])
                    ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php
TemplateManager::loadTemplate('/layout/footer.php');
?>
</body>
</html>
