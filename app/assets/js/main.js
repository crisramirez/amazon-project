function addToCart(id) {
    var count = $('#header .cart-count').text().trim();
    $.ajax({
        type: 'post',
        url: '/Amazon/app/buy/cart.php',
        data: {
            id: id
        },
        success: function () {
            $('#header .cart-count').text(++count);
        },
        error: function () {
            alert('there was an error');
        }
    });
}

function removeFromCart(id) {
    $.ajax({
        type: 'post',
        url: '/Amazon/app/buy/removeFromCart.php',
        data: {
            id: id
        },
        success: function (response) {
            try {
                response = JSON.parse(response);
            } catch (err) {
                alert('unknown error');
                return false;
            }
            var count = response.count;
            var price = response.total;

            $("#item-" + id).remove();
            $(".cart-count").text(count);
            $(".cart-price").text(price);

        },
        error: function () {
            alert('there was an error');
        }
    });
}

function deleteItem(id) {
    $.ajax({
        type: 'post',
        url: '/Amazon/app/my-store/delete.php',
        data: {
            id: id
        },
        success: function () {
            $('#item-' + id).remove();
        },
        error: function () {
            alert('there was an error');
        }
    });
}

/**
 * original value of the search box in header.php
 * it gets set on load (when document is ready)
 * @type {string}
 */
var originalValue = '';

/**
 * paginates the index page
 * it will set the value of input#page (an input field in header.php > form#filters-form
 * and then submit the form
 * @param page
 */
function paginate(page) {
    if (page == '') {
        return false;
    }
    // set value of input field
    $("#page").val(page);
    // submit the form
    $("#filters-form").submit();
}

/**
 * handles submission of form#filters-form
 * if originalValue is different than the current value in input#search
 * -- it will only submit input#search
 * also it will not submit any empty inputs
 */
function submitFilters() {
    // get the form jQuery object
    var form = $("#filters-form");
    // check if the value of input#search (the search box) has changed
    var queryChanged = originalValue != $("#search").val();

    // loop through all the elements that match the selector
    // @link https://api.jquery.com/category/selectors/ - selectors
    // @link http://api.jquery.com/jquery.each/         - jquery.each loops
    form.find('input').each(function () {
        // get id and value of the current element in the loop
        var id = $(this).attr('id');
        var v = $(this).val();

        // if queryChanged AND the id of the current element in the loop is not search
        // then do not submit this field - disabled it
        // NOTE: Disabled fields do not submit
        if (queryChanged && id != 'search') {
            $(this).prop("disabled", true);
        }
        // if value of current element is empty then disable this field (do not submit)
        else if (v == '') {
            $(this).prop("disabled", true);
        }
    });
}

function sort() {
    var selected = $('#sorting').find(":selected");
    var v = $(selected).attr('value');
    var o = $(selected).attr('data-order');
    $("#sort").val(v);
    $("#order").val(o);
    $("#filters-form").submit();
}

function filterPrice(from,to){
    $('#price_from').val(from);
    $('#price_to').val(to);
    $('#filters-form').submit();
}

function selectCategory(category) {
    if (page == '') {
        return false;
    }
    $('#category').val(category);
    $('#filters-form').submit();
}

function toggleFilters(btn) {
    $('#sidebar').toggleClass('hidden-xs');
    $(btn).find('i.fa').toggleClass('fa-caret-down fa-caret-up');
}

$(document).ready(function () {
    originalValue = $("#search").val();

    $('#search').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Amazon/app/items/search.php",
                // dataType: 'jsonp',
                data: {
                    q: request.term
                },
                success: function (data) {
                    var data = JSON.parse(data);
                    response(data);
                }
            });
        },
        focus: function (event, ui) {
            event.preventDefault;
            $(this).val(ui.item.label);
        },
        select: function (event, ui) {
            event.preventDefault;
            $(this).val(ui.item.label);
            window.location = ui.item.value;
        },
        delay: 750,
        minLength: 3
    });

    var selector = $('#recent-orders .order-details');
    if (selector.length > 0) {
        selector.off('show.bs.collapse').on('show.bs.collapse', function () {
            var $this = $(this);
            $this.closest('.panel').find('.toggle-icon').toggleClass('fa-caret-down fa-caret-up');
        });
        selector.off('hide.bs.collapse').on('hide.bs.collapse', function () {
            var $this = $(this);
            $this.closest('.panel').find('.toggle-icon').toggleClass('fa-caret-down fa-caret-up');
        });
    }
});