<?php
include_once '../../autoload.php';
require_once '../../vendor/autoload.php';

$id = $_GET['id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!empty($_POST['pw']) && !empty($_POST['pw_retype'])) {

        $password = md5($_POST['pw']);
        $password_retype = md5($_POST['pw_retype']);

        if ($password == $password_retype) {

            $sql = "UPDATE `users` SET `password` = ? WHERE `id` = ?";

            DB::execute($sql, [
                $password,
                $id
            ]);
        } else {
            flashMessage('Passwords dont match');
        }
    } else {
        flashMessage('Please enter a password');
    }
}


?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Log In | Amazon</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div class="container">
        <div class="row" style="margin-top: 20px;">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="well">
                    <h1>Password Recovery</h1>
                    <form action="" method="post" class="add-bottom">
                        <div class="form-group">
                            <label>New password:</label>
                            <input name="pw" type="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Re type password:</label>
                            <input name="pw_retype" type="password" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>

