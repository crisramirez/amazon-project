<?php
include_once '../../autoload.php';

$a = setcookie('user_id', -1, 1, COOKIE_DIR);
unset($_COOKIE['user_id']);

$_SESSION['user_id'] = null;
unset($_SESSION['user_id']);

session_destroy();

header("Location: ../index.php");
exit();

