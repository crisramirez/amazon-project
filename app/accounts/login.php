<?php
include_once '../../autoload.php';
require_once '../../vendor/autoload.php';

//handle login
$cond = isset($_POST['username']) && isset($_POST['password']);
if ($cond) {
    $username = $_POST['username'];
    $password = md5($_POST['password']);

    $sql = "SELECT `id` FROM `users` WHERE `username`= ? AND `password`= ?";
    $user = DB::fetch($sql, [
        $username,
        $password
    ]);

    if (!empty($user)) {
        if (isset($_POST['remember']) && $_POST['remember'] = 'accepted') {
            setcookie('user_id', $user['id'], time() + 60 * 60 * 24 * 30, COOKIE_DIR);
        } else {
            $_SESSION['user_id'] = $user['id'];
        }

        //redirect to index page
        header("Location: ../index.php");
        exit();
    } else {
        flashMessage('username and password are incorrect');
    }
}

// facebook login
$fb = new Facebook\Facebook([
    'app_id' => '186961091883904',
    'app_secret' => '7cfd7e02c045c4a17a84de29dea520c0',
    'default_graph_version' => 'v2.6',
    "persistent_data_handler" => "session"
]);
$helper = $fb->getRedirectLoginHelper();
$permissions = ['email', 'user_likes', 'publish_actions']; // optional
$loginUrl = $helper->getLoginUrl('http://localhost/Amazon/app/accounts/callback.php', $permissions);
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Log In | Amazon</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div class="container">
        <div class="row" style="margin-top: 20px;">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="well">
                    <?php
                    //check for the save flag set after registration
                    if (isset($_GET['save']) && $_GET['save'] == 'success'):
                        ?>
                        <div class="alert alert-success">Thanks for signing up! You can now log in.</div>
                    <?php endif; ?>

                    <h1>Log in</h1>
                    <form action="" method="post" class="add-bottom">
                        <div class="form-group">
                            <label>Username:</label>
                            <input name="username" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Password:</label>
                            <input name="password" type="password" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Log in</button>
                        &nbsp;&nbsp;
                        <input name="remember" type="checkbox" value="accepted"> Keep me logged in
                    </form>

                    <div>
                        <a href="<?= $loginUrl ?>">Login with Facebook</a>
                    </div>
                    <div>
                        Don't have an account yet? <a href="register.php">Join today</a>
                    </div>
                    <div>
                        <a href="" data-toggle="modal" data-target="#myModal"> Forgot your password</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Forgot password</h4>
                </div>
                <div class="modal-body">
                    <p>Please enter your email</p>
                </div>
                <form action="/Amazon/app/accounts/forgot-pass.php" method="post">
                    <input type="email" name="email">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>

