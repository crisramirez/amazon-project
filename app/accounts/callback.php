<?php
include_once '../../autoload.php';
require_once '../../vendor/autoload.php';

$fb = new Facebook\Facebook([
    'app_id' => '186961091883904',
    'app_secret' => '7cfd7e02c045c4a17a84de29dea520c0',
    'default_graph_version' => 'v2.6',
    "persistent_data_handler"=>"session"
]);

$helper = $fb->getRedirectLoginHelper();

try {
    $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

if (isset($accessToken)) {
    // Logged in!
    $response = $fb->get('/me?locale=en_US&fields=email,name,id', $accessToken);
    $userNode = $response->getGraphUser();

    $name = $userNode->getName();
    $email = $userNode->getEmail();
    $fbUserId = $userNode->getId();

    // check if this user has already logged in user facebook
    $sql = "SELECT * FROM `fb-login` WHERE `fb_id`= ?";
    $user = DB::fetch($sql, [$fbUserId]);

    if (empty($user)){
        // if user has never logged in using facebook
        // create new record in users table and fb-login table for next time this user comes in
        // insert record in `users` table
        // email = $email, username = preg_replace("/^[a-zA-Z]/", '', $name); $password = '' (empty string)
        $sql = "INSERT INTO `users` (`username`, `password`, `email`) VALUES (?, ?, ?)";

        // i have to revise the actual regex to remove all the non alpha characters, idk what it did but it only removed the first C lmao
        // why is it opening new windows?! wth....
        DB::execute($sql, [
            $name, //preg_replace("/^[a-zA-Z]/", '', $name),
            '',
            $email
        ]);
        // after you insert the record in the database
        $userId = DB::lastId(); // returns users.id
        // insert into fb-login -- save users.is in fb-login.user_id
        // user id = $userId
        // fb_id = $fbUserId
        $sql = "INSERT INTO `fb-login` (`fb_id`, `user_id`) VALUES (?, ?)";
        DB::execute($sql, [
           $fbUserId,
            $userId
        ]);
    } else {
        // if this user has logged in with facebook before, use their user_id from fb-login table
        // which is the same id as users.id (users.id === fb-login.user_id)
        $userId = $user['user_id'];
    }
    $_SESSION['user_id'] = $userId;
}

header("Location: ../index.php");
exit();

