<?php
include_once '../../autoload.php';
require_once '../../vendor/autoload.php';

$cond = isset($_POST['username']) && isset($_POST['email']) && isset($_POST['password']);

if ($cond) {
    if (empty($_POST['username']) || empty($_POST['email']) || empty($_POST['password'])) {
        errorMsg('Error: Enter all required fields');
    } else {
        //check for unique username and email
        $username = $_POST['username'];
        $sql = "SELECT * FROM `users` WHERE `username`= ?";
        $result = DB::execute($sql, [$username]);

        $email = $_POST['email'];
        $sql_email = "SELECT * FROM `users` WHERE `email`= ?";
        $result_email = DB::execute($sql_email, [$email]);

        if ($result->rowCount() != 0) {
            flashMessage('Username already exists');
        } elseif ($result_email->rowCount() != 0) {
            flashMessage('Email already exists');
        } else {
            //save to db
            $pw = md5($_POST['password']);
            $sql = "INSERT INTO `users` (`username`, `password`, `email`) VALUES (?, ?, ?)";
            DB::execute($sql, [
                $username,
                $pw,
                $email
            ]);

            //redirect to login page with save flag at the end to display the message
            header("Location: login.php?save=success");
            exit();
        }
    }
}

$fb = new Facebook\Facebook([
    'app_id' => '186961091883904',
    'app_secret' => '7cfd7e02c045c4a17a84de29dea520c0',
    'default_graph_version' => 'v2.6',
    "persistent_data_handler" => "session"
]);
$helper = $fb->getRedirectLoginHelper();
$permissions = ['email', 'user_likes', 'publish_actions']; // optional
$loginUrl = $helper->getLoginUrl('http://localhost/Amazon/app/accounts/callback.php', $permissions);
?>


<html>
<head>
    <meta charset="UTF-8">
    <title>Register | Amazon</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div class="container">
        <div class="row" style="margin-top: 20px;">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="well">
                    <h1>Join Today</h1>
                    <form action="" method="post" class="add-bottom">
                        <div class="form-group">
                            <label> Email:</label>
                            <input name="email" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Username:</label>
                            <input name="username" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Password:</label>
                            <input name="password" type="password" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">Create Account</button>
                    </form>

                    <div>
                        <a href="<?= $loginUrl ?>">Login with Facebook</a>
                    </div>

                    <div>
                        Already have an account? <a href="login.php">Log in</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>