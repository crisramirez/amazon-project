<?php

include_once '../../autoload.php';

$q = $_GET['q'];
$sql = "SELECT `name`,`id` FROM `catalog` WHERE `name` LIKE ? AND `deleted` = '0' LIMIT 10";

$items = DB::fetchAll($sql, [
    '%' . $q . '%'
]);

// convert this array into a simpler array with only
// ['label' => <item name>, 'value' => <url to item>
$response = [];

foreach ($items as $item) {
    $response[] = [
        'label' => $item['name'],
        'value' => '/Amazon/app/items/item.php?id=' . $item['id']
    ];
}

echo json_encode($response);
