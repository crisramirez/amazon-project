<?php
include_once '../../autoload.php';

$id = $_GET['id'];
$sql = "SELECT * FROM `catalog` WHERE `id` = ?";
$item = DB::fetch($sql, [$id]);

$owner = !User::isGuest() && User::getCurrentUserId() == $item['user_id'];
?>

<html>
<head>
    <meta charset="UTF-8">
    <title><?= $item['name'] ?> | Amazon</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div class="container">
        <div class="row">
            <div class="col col-sm-6 col-lg-6">
                <div class="cart-item add-bottom">
                    <div>
                        <h1 class="no-top"><?= $item['name'] ?></h1>
                        <div class="image-wrapper image-wrapper-lg text-center">
                            <?php
                            list($w, $h) = getimagesize(CDN . 'images/' . $item['image']);

                            $imgClass = $w > $h ? 'landscape-img' : 'portrait-img';
                            ?>
                            <img id="img-<?= $item['id'] ?>" class="<?= $imgClass ?>" src="/Amazon/app/assets/images/<?= $item['image'] ?>">
                        </div>
                        <p><?= nl2br($item['description']) ?></p>
                    </div>
                    <?php
                    TemplateManager::loadTemplate('/items/partials/price-info.php', [
                        'owner' => $owner,
                        'ownerCtrls' => true,
                        'item' => $item
                    ]);
                    ?>
                </div>
            </div>
        </div>

        <?php
        if ($owner) {
            TemplateManager::loadTemplate('/items/partials/delete-dialog.php', [
                'item' => $item,
                'ajax' => false
            ]);
        }
        ?>
    </div>
    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>