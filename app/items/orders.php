<?php
include_once '../../autoload.php';

if (User::isGuest()) {
    throw new Exception('Please login');
}

$userId = User::getCurrentUserId();
$sql = "SELECT * FROM `transactions` WHERE `user_id` = ?";
$orders = DB::fetchAll($sql, [
    $userId
]);

$inCart = false;

?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Orders | Amazon</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div class="container">
        <h1>Recent Orders</h1>
        <div id="recent-orders">
            <?php if (count($orders) > 0): ?>
                <?php foreach ($orders as $order): ?>
                    <?php $transId = $order['id']; ?>
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title clear">
                                    <a class="no-underline order-details-toggle" data-toggle="collapse" data-parent="#accordion" href="#<?= $order['id']; ?>">
                                        <i class="fa fa-caret-down toggle-icon"></i>
                                        <span><?= toDollars($order['total']) ?></span>

                                        <span class="pull-right">
                                    <?php
                                    $date = date_create($order['date']);
                                    echo date_format($date, 'l jS \o\f F Y h:i A');
                                    ?>
                                </span>
                                    </a>
                                </h4>
                            </div>
                            <div id="<?= $order['id']; ?>" class="panel-collapse collapse order-details">
                                <div class="panel-body">
                                    <?php
                                    $sqlNames = "SELECT * FROM catalog 
                                    INNER JOIN transaction_items ON catalog.id = transaction_items.item_id 
                                    WHERE transaction_id = ?";
                                    $items = DB::fetchAll($sqlNames, [
                                        $transId
                                    ]);
                                    ?>
                                    <div class="well">
                                        <h4>Order Details</h4>
                                        <?php
                                        $count = 0;
                                        foreach ($items as $item) {
                                            $count += $item['count'];
                                            TemplateManager::loadTemplate('/items/list.php', [
                                                'item' => $item,
                                                'inCart' => $inCart
                                            ]);
                                        }
                                        ?>
                                        <div class="clear add-top">
                                            <?php
                                            TemplateManager::loadTemplate('/items/partials/total-price-info.php', [
                                                'total' => $order['total'],
                                                'count' => $count,
                                                'inCart' => $inCart
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <h3>Nothing to see here</h3>
                <a href="../">Continue shopping</a>
            <?php endif; ?>
        </div>

    </div>
    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>

