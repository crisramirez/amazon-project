<?php
include_once '../autoload.php';

$page = getPage();

$data = getItems($_GET, $page);

$items = $data['items'];
$lastPage = $data['lastPage'];
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Amazon | Shopping!</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div id="content" class="container">
        <div class="row">
            <div class="col-sm-3">
                <?php
                TemplateManager::loadTemplate('/layout/sorting.php', [
                    'data' => $_GET
                ]);
                ?>
            </div>
            <div class="col-sm-9">
                <div id="items-grid" class="row">
                    <?php
                    foreach ($items as $item) {
                        TemplateManager::loadTemplate('/items/grid.php', [
                            'item' => $item
                        ]);
                    }
                    ?>
                </div>
                <div class="add-top add-bottom clear">
                    <?php
                    TemplateManager::loadTemplate('/layout/pagination.php', [
                        'page' => $page,
                        'lastPage' => $lastPage
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>
