<?php

include_once '../../autoload.php';

// get the item you want to remove
$itemToRemove = $_POST['id'];

// create an empty array
// loop through your cart, any ids that ARE NOT $itemToRemove add to the new array you created
// then set session equals this new array
$newCart = [];
foreach ($_SESSION['cart'] as $itemToKeep) {
    if ($itemToKeep != $itemToRemove) {
        array_push($newCart, $itemToKeep);
    }
}


// After the loop make $_SESSION['cart'] = <temp array>
$_SESSION['cart'] = $newCart;

// get all you cart data
// you only want to return the cart's total quantity and the formatted total price
$data = getCart($_SESSION['cart']);
$resp = [
    'count' => $data['count'],
    'total' => toDollars($data['total'])
];

// you will use this JSON in your javascript to update the top-right corner
// because when you remove an item you remove ALL the times it was added to the cart
// you cant do 'count--' in your javascript because you are potentially removing more than one element
$response = json_encode($resp);
echo $response;

