<?php
include_once '../../autoload.php';

/*
 * 1) create your check out page
 *      -display a quick list of what you are buying
 *          -very similar to cart.php but without the image
 *          ------------------------------------------------------------------------
 *          | item 1 name                                       | price | quantity |
 *          ------------------------------------------------------------------------
 *          | item 2 name                                       | price | quantity |
 *          ------------------------------------------------------------------------
 *          ....
 *          ------------------------------------------------------------------------
 *          | total                                             | price | quantity |
 *          ------------------------------------------------------------------------
 *  2) integrate with Stripe.js library to generate the checkout page
 */
if (empty($_SESSION['cart'])) {
    exit("empty cart");
} else {
    $data = getCart($_SESSION['cart']);
    $items = $data['items'];
    $total = $data['total'];
    $count = $data['count'];
}

$inCart = false;
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Checkout | Amazon</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    <?php if (empty($items)): ?>
                        <h3>No items in your cart</h3>
                        <a href="../">Continue shopping</a>
                    <?php else: ?>
                        <?php
                        foreach ($items as $item) {
                            TemplateManager::loadTemplate('/items/list.php', [
                                'item' => $item,
                                'inCart' => $inCart
                            ]);
                        }
                        ?>
                        <div class="clear add-top">
                            <?php
                            TemplateManager::loadTemplate('/items/partials/total-price-info.php', [
                                'total' => $total,
                                'count' => $count,
                                'inCart' => $inCart
                            ]);
                            ?>
                            <form action="/Amazon/app/buy/pay.php" method="POST" class="text-right no-bottom">
                                <script
                                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                        data-key="pk_test_6pRNASCoBOKtIshFeQd4XMUh"
                                        data-amount="<?= $total ?>"
                                        data-name="Amazon"
                                        data-description="Widget"
                                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                        data-locale="auto"
                                        data-zip-code="true">
                                </script>
                            </form>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>


