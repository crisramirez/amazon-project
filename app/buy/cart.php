<?php

include_once '../../autoload.php';

//initialize the session variable if it doesn't exist
if (!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = [];
}

// this file will be used for both handling the form submission (POST) and to display your cart (GET)
// so you will have to add a check for whether you are POSTING or GETTING to this file
// bing: how to check if POSTing PHP
// watch: https://www.youtube.com/watch?v=CEl3bhCBjtI (NOTE: I haven't actually seen the video so i hope it's good)
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = $_POST['id'];
    array_push($_SESSION['cart'], $id);
    exit();
}

// we know $_SESSION['cart'] exists because we create it so no need to check
$data = getCart($_SESSION['cart']);

// create the variables you used in the front end
$items = $data['items'];
$total = $data['total'];
$count = $data['count'];

// ok so now you have an array with all your items' info and their count and a variable with the total amount ($$$)
// so you can begin your html
// your html should have the same layout (header) on top and then a grid with an item per line like this:
// -------------------------------------------------------------------------------------------------------------------
// |            |   item name here .......                                                                           |
// | item image |   price per item                                                                 quantity per item |
// |            |                                                                                                    |
// -------------------------------------------------------------------------------------------------------------------
// then at the better end display the total price and a checkout button!
// have fun!

$inCart = true;
?>


<html>
<head>
    <meta charset="UTF-8">
    <title>Cart | Amazon</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    <?php if (empty($items)): ?>
                        <h3>No items in your cart</h3>
                        <a href="../">Continue shopping</a>
                    <?php else: ?>
                        <?php
                        foreach ($items as $item) {
                            TemplateManager::loadTemplate('/items/list.php', [
                                'item' => $item,
                                'inCart' => $inCart
                            ]);
                        }
                        ?>
                        <div class="clear add-top">
                            <?php
                            TemplateManager::loadTemplate('/items/partials/total-price-info.php', [
                                'total' => $total,
                                'count' => $count,
                                'inCart' => $inCart
                            ]);
                            ?>
                            <a href="/Amazon/app/buy/checkout.php">
                                <button type="button" class="pull-right btn btn-primary">
                                    Checkout
                                </button>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>