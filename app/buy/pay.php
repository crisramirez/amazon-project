<?php

include_once '../../autoload.php';
require_once '../../vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// 1) save transaction record
// -- check if you are logged in, and save user_id if you are
// search: how to get the last inserted id in php/mysql to get the transaction_id
// 2) save one record per item in the transaction_item table
// 3) send email
// 4) delete everything in the cart
// 5) redirect to confirmation.php

\Stripe\Stripe::setApiKey(STRIPE['secret_key']);

// check if it's a post request
// if its not, return error "method not supported"
if (count($_POST) == 0) {
    throw new \Exception('Method not allowed');
}

$token = $_POST['stripeToken'];
$email = $_POST['stripeEmail'];

if (empty($_SESSION['cart'])) {
    throw new \Exception("Empty cart");
} else {
    $data = getCart($_SESSION['cart']);
    $items = $data['items'];
    $total = $data['total'];
    $count = $data['count'];
}

// get the email from the posted data
// hint: use print_r and then exit to check what's inside an array if you don't know
// get the total amount - this may be part of the form idk
// if not you can get it from $_SESSION like you have done til now
// charge your customer
// refer to the Stripe docs
$customer = \Stripe\Customer::create([
    'email'  => $email,
    'source' => $token
]);


// do a try-catch to see if there are any errors when you charge
// watch video on try-catch and exceptions
// https://www.youtube.com/watch?v=0l7IjO33vHM (continue to 186 and 187 ... maybe more)
// if there are no errors, check the status of the charge
// if it succeded then continue to the next steps
// -- else check in the charge object for any errors
// -- refer to: https://stripe.com/docs/api/php#charge_object for more info
try {
    $charge = \Stripe\Charge::create([
        'customer' => $customer->id,
        'amount'   => $total,
        'currency' => 'usd'
    ]);
} catch (Exception $e) {
    throw new \Exception('Error charging card: ' . $e->getMessage() . "\n");
}

if ($charge['status'] == 'failed') {
    throw new \Exception('Card couldn\'t be charged');
}

// if the transaction went through just fine then
// create a new entry in the transactions table
//      this is the general info of the transaction
// and entries in the transaction_items table
//      this is an entry per item in the transaction
$id = $charge['id'];
$userId = User::getCurrentUserId();

if ($userId != null) {
    $sql = "INSERT INTO transactions (`user_id`, `stripe_id`, `total`, `email`) VALUES (?,?,?,?)";
    $array = [$userId,$id,$total,$email];
} else {
    $sql = "INSERT INTO transactions (`stripe_id`, `total`, `email`) VALUES (?,?,?)";
    $array = [$id,$total,$email];
}
DB::execute($sql,$array);

$transId = DB::lastId();

// insert into the table transaction items using the transaction id and item id
foreach ($items as $item) {

    // insert a row per each time the item was bough
    // each $item has the element 'count' which is how many times you bought that item
    $count = $item['count']; // how many times you bought this item
    $itemPrice = $item['price'];
    $itemId = $item['id'];

    $sql = "INSERT INTO transaction_items (`transaction_id`,`item_id`,`price_paid`,`count`) VALUES (?,?,?,?)";
    DB::execute($sql,[
        $transId,$itemId,$itemPrice,$count
    ]);
}

$emailBody = 'Thank you for your purchase';

$subject = 'Confirmation';
sendEmail($email,$emailBody,$subject);

unset($_SESSION['cart']);
header('Location: confirmation.php?id=' . $transId);
exit();