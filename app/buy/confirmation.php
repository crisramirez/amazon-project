<?php
include_once '../../autoload.php';

$id = $_GET['id'];

$sql = "SELECT * FROM catalog 
INNER JOIN transaction_items ON catalog.id = transaction_items.item_id 
WHERE transaction_id = ?";
$items = DB::fetchAll($sql, [$id]);

$sqlTotal = "SELECT * FROM transactions WHERE id = ?";
$resultTotal = DB::fetch($sqlTotal, [$id]);
$total = $resultTotal['total'];

$inCart = false;
?>

<html>
<head>
    <meta charset="UTF-8">
    <title>Confirmation | Amazon</title>
    <?php
    TemplateManager::loadTemplate('/layout/head.php');
    ?>
</head>
<body>
<div id="wrap">
    <?php
    TemplateManager::loadTemplate('/layout/header.php');
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="jumbotron text-center">
                    <h2>Thank you for your purchase!</h2>
                </div>
                <div class="well">
                    <?php
                    $count = 0;
                    foreach ($items as $item) {
                        $count += $item['count'];
                        TemplateManager::loadTemplate('/items/list.php', [
                            'item' => $item,
                            'inCart' => $inCart
                        ]);
                    }
                    ?>
                    <div class="clear add-top">
                        <?php
                        TemplateManager::loadTemplate('/items/partials/total-price-info.php', [
                            'total' => $total,
                            'count' => $count,
                            'inCart' => $inCart
                        ]);
                        ?>
                        <a href="/Amazon/app">
                            <button type="button" class="pull-right btn btn-primary">
                                Back to shopping
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    TemplateManager::loadTemplate('/layout/footer.php');
    ?>
</div>
</body>
</html>
