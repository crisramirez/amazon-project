<?php

echo PHP_EOL;

// for mac only
exec('rm -R ../app/assets/images/');
exec('mkdir ../app/assets/images/');
exec('chmod -R 0777 ../app/assets/images/');

// for windows only
//exec('rd ..\app\assets\images');
//exec('mkdir ..\app\assets\images');

$file = file_get_contents('MOCK_DATA.json');
$items = json_decode($file, true);
$categories = [
    'Beauty',
    'Outdoors',
    'Computers',
    'Clothing',
    'Industrial',
    'Music',
    'Tools',
    'Computers',
    'Jewelery',
    'Kids'
];

// this will reset your entire database except for the users

$db = mysqli_connect('127.0.0.1', 'root', '', 'Amazon') or die('oops!');
mysqli_begin_transaction($db);

// delete all data in tables
$query = "DELETE FROM `catalog`";
mysqli_query($db, $query);
$query = "DELETE FROM `transactions`";
mysqli_query($db, $query);
$query = "DELETE FROM `transaction_items`";
mysqli_query($db, $query);

$imageCache = [];
foreach ($items as $item) {
    $n = $item['name'];
    $d = $item['description'];
    $p = rand(500, 99999);
    $c = $categories[rand(0, count($categories) - 1)];

    // try to get an image url that works
    // if http code is not 200 then try again
    $i = getImage();
    $hash = md5($i);
    $dest = $hash . '.jpeg';

    if (!isset($imageCache[$hash])) {
        $k = 0;
        while (!checkUrlExists($i) && $k < 10) {
            $i = getImage();
            $hash = md5($i);
            $dest = $hash . '.jpeg';
            $k++; // only attempt this 10 times
        }
        downloadImage($i, '../app/assets/images/'.$dest);
        $imageCache[$hash] = $dest;
    }

    $query = "INSERT INTO `catalog` (`name`, `description`, `price`, `category`, `image`) VALUES ('{$n}', '{$d}', '{$p}', '{$c}', '{$dest}')";
    mysqli_query($db, $query) or die(mysqli_error($db));

    if (mysqli_errno($db) > 0) {
        $err = mysqli_error($db);
        mysqli_rollback($db);
        exit($err);
    }
    echo '+';
}
mysqli_commit($db);

/**
 * @return string
 */
function getImage()
{
    $ratios = [
        [480, 640],
        [480, 480],
        [640, 480]
    ];
    $i = rand(3, 10);
    $ratio = $ratios[$i % 3];
    $w = $ratio[0];
    $h = $ratio[1];
    $url = "http://lorempixel.com/{$w}/{$h}/transport/{$i}/cc/";
    return $url;
}

/**
 * checks if the URL returns http code 200
 * see @https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 * @param string $url
 * @return bool
 */
function checkUrlExists($url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
    curl_setopt($ch, CURLOPT_NOBODY, true);    // we don't need body
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return $httpCode == 200;
}

function downloadImage($url, $destination)
{
    $ch = curl_init($url);
    $fp = fopen($destination, "wb");

// set URL and other appropriate options
    $options = array(
        CURLOPT_FILE           => $fp,
        CURLOPT_HEADER         => 0,
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_TIMEOUT        => 60
    ); // 1 minute timeout (should be enough)

    curl_setopt_array($ch, $options);

    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
}