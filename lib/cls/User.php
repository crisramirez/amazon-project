<?php

/**
 * Class User
 */
class User
{
    /**
     * @return bool
     */
    public static function isGuest()
    {
        return !isset($_SESSION['user_id']) && !isset($_COOKIE['user_id']);
    }

    /**
     * @return int|null
     */
    public static function getCurrentUserId()
    {
        if (self::isGuest()) {
            return null;
        }

        return isset($_COOKIE['user_id']) ? $_COOKIE['user_id'] : $_SESSION['user_id'];
    }

    /**
     * @return array|null
     */
    public static function getUser()
    {
        $id = self::getCurrentUserId();
        if ($id == null) {
            return null;
        }

        $query = "SELECT * FROM `users` WHERE `id` = '{$id}'";
        return DB::fetch($query);
    }
}