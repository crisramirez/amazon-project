<?php

/**
 * Class TemplateManager
 */
class TemplateManager
{
    /**
     * @var string
     */
    const TEMPLATE_DIR = 'templates';

    /**
     * @param string $file
     * @param array $args
     */
    public static function loadTemplate($file, array $args = [])
    {
        foreach ($args as $varName => $varValue) {
            $$varName = $varValue;
        }

        $relative = self::getRelativePath();
        $path = $relative . self::TEMPLATE_DIR . DIRECTORY_SEPARATOR . ltrim($file, DIRECTORY_SEPARATOR);
        include $path;
    }

    /**
     * @return string
     */
    private static function getRelativePath()
    {
        $root = $_SERVER['DOCUMENT_ROOT'] . '/Amazon';
        $relative = getcwd();

        $segments = explode('/', $root);
        $segments2 = explode(DIRECTORY_SEPARATOR, $relative);

        $upLevel = count($segments2) - count($segments);

        return str_repeat('../', $upLevel);
    }
}