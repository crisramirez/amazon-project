<?php

/**
 * Class DB
 */
class DB
{
    /**
     * @var mysqli
     */
    public static $connection = null;

    public static function getConnection()
    {
        $host = DB_HOST;
        $name = DB_NAME;

        if (self::$connection == null) {
            self::$connection = new PDO("mysql:host=$host;dbname=$name", DB_USERNAME, DB_PW);
        }
        return self::$connection;
    }

    public static function lastId()
    {
        return self::getConnection()->lastInsertId();
    }

    /**
     * @param $query
     * @param array $params
     * @return PDOStatement
     * @throws PDOException instead of handle exceptions in here, throw the exception
     *                      and let the script or page calling this function handle the error
     *                      this makes the class more predictable and allows for different scripts/pages to handle
     *                      the error differently for different purposes
     */
    public static function execute($query, array $params)
    {
        $conn = self::getConnection();
        $stmt = $conn->prepare($query);
        if (!$stmt->execute($params)) {
            // NOTE: PDOStatement::errorInfo() returns an array [<error code>, <driver error code>, <Driver specific error message>]
            // throw an exception where the message is the error message from the query
            throw new PDOException($stmt->errorInfo()[2]);
        }

        return $stmt;
    }

    /**
     * @param string $query
     * @return array
     */
    public static function fetchAll($query, $params)
    {
        $result = self::execute($query, $params);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $query
     * @return array|null
     */
    public static function fetch($query, $params)
    {
        $result = self::execute($query, $params);
        return $result->fetch(PDO::FETCH_ASSOC);
    }
}