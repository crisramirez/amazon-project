<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * @param string $message
 */
function errorMsg($message)
{
    echo '<div class="add-padding-top add-padding-bottom text-center bg-danger text-danger">' . $message . '</div>';
}

/**
 * @return int
 */
function getPage()
{
    $page = isset($_GET['page']) ? $_GET['page'] : 1;
    if (!is_numeric($page)) {
        return 1;
    }
    $page = intval($page);
    return $page > 1 ? $page : 1;
}

/**
 * gets cart items, total count of items in the cart and total price of cart
 *
 * @param array $cart list of ids in the cart
 * @return array ['total' => <total price>, 'count' => <total count of items>, 'items' => <items in cart | straight form database>]
 */
function getCart(array $cart)
{
    // get how many times each id appears in the cart
    // returns [<id> => <count of times this ids appears>, ...]
    $countPerItem = array_count_values($cart);

    // init your total to zero
    $total = 0;

    // init your count to zero
    $count = 0;

    $items = [];
    // if the cart is not empty grab their info from database
    if (!empty($cart)) {
        // query the database using IN clause
        // @see https://www.w3schools.com/Sql/sql_in.asp
        $ids = str_repeat('?,', count($cart) - 1) . '?';
        $sql = "SELECT * FROM `catalog` WHERE `id` IN({$ids})";
        $items = DB::fetchAll($sql, $cart);

        foreach ($items as &$item) {
            // get the count per item for this $id ($item['id'])
            $item['count'] = $countPerItem[$item['id']]; // we are adding 'count' here but we never reassign it back to the $items array
            // so that change never takes place
            // there are two ways to do it

            // update the global count
            $count += $item['count'];

            // update the global total
            $total += $item['count'] * $item['price'];
        }
    }


    // return your data
    return [
        'total' => $total,
        'count' => $count,
        'items' => $items
    ];
}

/**
 * @param array $data
 * @param int $page
 * @param int $userId
 * @return array
 */
function getItems(array $data, $page = 1, $userId = null)
{
    $itemPerPage = PAGE_SIZE;
    $offset = ($page - 1) * $itemPerPage;

    $sql = "SELECT * FROM `catalog` WHERE `deleted` = '0'";
    $params = []; // create empty array

    // check for user id
    if ($userId != null) {
        // it used to be this:
        // $sql .= " AND `user_id` = {$userId}";
        $sql .= " AND `user_id` = ?"; // substitute my variable for a ?
        $params[] = $userId; // add my varible that was in the query to my array of parameters i created ($params)
    }

    // search
    if (isset($data['q']) && !empty($data['q'])) {
        $q = $data['q'];
        $sql .= " AND `name` LIKE ?";
        $params[] = '%' . $q . '%';
    }

    // filters
    // price lower bound
    if (isset($data['price_from']) && !empty($data['price_from'])) {
        $priceFrom = $data['price_from'] * 100;
        $sql .= " AND `price` >= ?";
        $params[] = $priceFrom;
    }

    // price upper bound
    if (isset($data['price_to']) && !empty($data['price_to'])) {
        $priceTo = $data['price_to'] * 100;
        $sql .= " AND `price` <= ?";
        $params[] = $priceTo;
    }

    // category
    if (isset($data['category']) && !empty($data['category'])) {
        $category = $data['category'];
        $sql .= " AND `category` = ?";
        $params[] = $category;
    }

    // sort
    // <condition> ? <value if it's true> : <value if it's false>
    $sort = isset($data['sort']) ? $data['sort'] : 'name';
    $order = isset($data['order']) ? $data['order'] : 'asc';

    $sql .= " ORDER BY `$sort` $order";

    // get number of items before we impose limits
    $resultTotal = DB::execute($sql, $params);
    $count = $resultTotal->rowCount();
    $lastPage = ceil($count / $itemPerPage);

    $sql .= " LIMIT {$itemPerPage} OFFSET {$offset}";
    $items = DB::fetchAll($sql, $params);

    return [
        'items' => $items,
        'lastPage' => $lastPage
    ];
}

/**
 * @param int $amount
 * @return string
 */
function toDollars($amount)
{
    if (empty($amount)) {
        return '';
    }
    return '$' . number_format($amount / 100, 2);
}

/**
 * @param string $string
 * @return string
 */
function slug($string)
{
    return strtolower(preg_replace(array('/[^-a-zA-Z0-9\s]/', '/[\s]/'), array('', '-'), $string));
}

/**
 * @param string $path
 * @return string
 */
function transformImageName($path)
{
    $pathInfo = pathinfo($path);
    $ext = $pathInfo['extension'];
    $name = $pathInfo['filename'];
    return slug($name) . '.' . $ext;
}

function sendEmail($email,$emailBody,$subject){

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = EMAIL_USERNAME;                     // SMTP username
        $mail->Password = EMAIL_PW;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
        //Recipients
        $mail->setFrom('man.super1999@gmail.com', 'Cris');
        $mail->addAddress($email);
        $mail->addReplyTo('man.super1999@gmail.com', 'Cris');

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body = $emailBody;
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();

    } catch (Exception $e) {
        throw $e;
    }
}

function  flashMessage($message, $type = MSG_ERROR) {
    // create empty arrays if session messages or session messages type don't exists yet
    if (!isset($_SESSION['message'])) {
        $_SESSION['message'] = [];
    }
    if (!isset($_SESSION['message'][$type])) {
        $_SESSION['message'][$type] = [];
    }

    // add element to list of messages
    $_SESSION['message'][$type][] = $message;
}

?>