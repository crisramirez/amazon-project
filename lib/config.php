<?php

session_start();

// define public constants
const BASE_URL = 'http://localhost/Amazon/app/';
const CDN = 'http://localhost/Amazon/app/assets/';
const COOKIE_DIR = '/Amazon/app/';

const DEFAULT_SEARCH_URL = '/Amazon/app/index.php';
const PAGE_SIZE = 16;

const MSG_INFO = 'info';
const MSG_SUCCESS = 'success';
const MSG_WARNING = 'warning';
const MSG_ERROR = 'danger';

const CATEGORIES = [
    'Beauty',
    'Outdoors',
    'Computers',
    'Clothing',
    'Industrial',
    'Music',
    'Tools',
    'Jewelery',
    'Kids'
];

const SORT_OPTIONS = [
    ['name', 'asc', 'Name A-Z'],
    ['name', 'desc', 'Name Z-A'],
    ['price', 'asc', 'Price Low-High'],
    ['price', 'desc', 'Price High-Low']
];